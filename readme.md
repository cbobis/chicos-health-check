Chico's Health Check
====================

The health check app is an App Engine application written in Python and utilizing App Engine�s Cron Service to regularly monitor Chico�s applications.

settings.yaml hold login credentials and a list of email addresses that needs to be notified on certain events

Sample contents:
login:
- user: sampleuser
  email: sampleuser@example.com
  password: SamplePass1!

notifications:
- email: samplenotifmail1@example.com
  event: password_expired
  cc: samplenotifmail2@example.com
  
This is the master branch
