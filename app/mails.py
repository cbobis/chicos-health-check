from ferris import settings
from google.appengine.api import mail, app_identity
from app.settings import settings
from app.models.health_check import HealthCheck
import datetime
import logging

PORTAL_URL = settings['chicos_urls']['portal']
SCORECARD_URL = settings['chicos_urls']['scorecard']
APP_ID = app_identity.get_application_id()
MAILER = 'noreply@%s.appspotmail.com' % APP_ID

class SendMail:
    def __init__(self, mail_type, **kwargs):
        getattr(self, mail_type)(**kwargs)


    @staticmethod
    def password_expired(email=None, username=None, cc=None):
        recipient = email
        sender = MAILER
        subject = "Chico's Heath Check - Account Password Expired"
        msg_body = """<html>
    <body>
        <p>Dear %s,</p>
        <p>Unfortunately, your account's password to Chico's Vendor Extranet had expired.</p>
        <p>Kindly reset your password to continue with the Heath Check</p>
        <br>
        <p>To login, please proceed to this link: <a href="%s/users/login">%s/users/login</a></p>
        <br>
        <p>Thank you!</p>
        <p><em>Chico's FAS, Inc.</em></p>
        <hr>
        <p><em>This is an auto-generated email, please don't reply.</em></p>
    </body>
    </html>"""
        msg_body = msg_body % (username,PORTAL_URL,PORTAL_URL)

        _prep_send_mail(email, 'password_expired', recipient, sender, subject, msg_body, cc)

    @staticmethod
    def account_inaccessible(email=None, username=None, content=None, cc=None, site='portal'):
        recipient = email
        sender = MAILER
        subject = "Chico's Heath Check - Account Not Accessible"
        if site == 'portal':
            application = 'Extranet Portal'
        else: application = 'Scorecard Application'
        msg_body = """<html>
    <body>
        <p>Dear %s,</p>
        <p>Unfortunately, your account has no access to Chico's Vendor %s.</p>
        <p>Kindly verify your account.</p>
        <br>
        <p>If you have any questions, concerns, or clarifications, please contact this email: <a href="mailto:csr@chicos.com">csr@chicos.com</a></p>
        <br>
        <p>Thank you!</p>
        <p><em>Chico's FAS, Inc.</em></p>
        <hr>
        <p><em>This is an auto-generated email, please don't reply.</em></p>
    </body>
    </html>"""
        msg_body = msg_body % (username, application)
        msg_body += content

        _prep_send_mail(email, 'account_inaccessible', recipient, sender, subject, msg_body, cc)


    @staticmethod
    def portal_inaccessible(email=None, content=None, cc=None):
        recipient = email
        sender = MAILER
        subject = "Chico's Heath Check - Portal Not Accessible!"
        msg_body = """<html>
    <body>
        <p>Hello,</p>
        <p>Chico's Vendor Extranet Portal is currently not accessible.</p>
        <p>See Portal Response below.</p>
        <br>
        <p><em>Chico's FAS, Inc.</em></p>
        <hr>
        <p><em>This is an auto-generated email, please don't reply.</em></p>
    </body>
    </html>"""
        msg_body += content

        _prep_send_mail(email, 'portal_inaccessible', recipient, sender, subject, msg_body, cc)

    @staticmethod
    def portal_pages_not_found(email=None, cc=None):
        recipient = email
        sender = MAILER
        subject = "Chico's Heath Check - No Pages Found!"
        msg_body = """<html>
    <body>
        <p>Hello,</p>
        <p>There are currently no available pages found on Chico's Vendor Extranet Portal.</p>
        <br>
        <p><em>Chico's FAS, Inc.</em></p>
        <hr>
        <p><em>This is an auto-generated email, please don't reply.</em></p>
    </body>
    </html>"""

        _prep_send_mail(email, 'portal_pages_not_found', recipient, sender, subject, msg_body, cc)

    @staticmethod
    def scorecard_inaccessible(email=None, content=None, cc=None):
        recipient = email
        sender = MAILER
        subject = "Chico's Heath Check - Scorecard Application Not Accessible!"
        msg_body = """<html>
    <body>
        <p>Hello,</p>
        <p>Chico's Vendor Scorecard Application is currently not accessible.</p>
        <p>See Portal Response below.</p>
        <br>
        <p><em>Chico's FAS, Inc.</em></p>
        <hr>
        <p><em>This is an auto-generated email, please don't reply.</em></p>
    </body>
    </html>"""
        msg_body += content

        _prep_send_mail(email, 'scorecard_inaccessible', recipient, sender, subject, msg_body, cc)

    @staticmethod
    def scorecard_records_not_found(email=None, cc=None):
        recipient = email
        sender = MAILER
        subject = "Chico's Heath Check - No Pages Found!"
        msg_body = """<html>
    <body>
        <p>Hello,</p>
        <p>There are currently no available records found on Chico's Vendor Scorecard Application.</p>
        <br>
        <p><em>Chico's FAS, Inc.</em></p>
        <hr>
        <p><em>This is an auto-generated email, please don't reply.</em></p>
    </body>
    </html>"""
        msg_body = msg_body

        _prep_send_mail(email, 'scorecard_records_not_found', recipient, sender, subject, msg_body, cc)

    @staticmethod
    def request_timeout(email=None, cc=None, site=None, url=None):
        recipient = email
        sender = MAILER
        subject = "Chico's Heath Check - No Pages Found!"
        if site == 'portal':
            application = 'Extranet Portal'
        else: application = 'Scorecard Application'
        msg_body = """<html>
    <body>
        <p>Hello,</p>
        <p>Chico's Vendor %s took too long to respond.</p>
        <p>Url requested on Health Check: <a href="%s">%s</a></p>
        <br>
        <p><em>Chico's FAS, Inc.</em></p>
        <hr>
        <p><em>This is an auto-generated email, please don't reply.</em></p>
    </body>
    </html>"""
        msg_body = msg_body % (application,url,url)

        _prep_send_mail(email, 'request_timeout', recipient, sender, subject, msg_body, cc)

def _prep_send_mail(email, event, recipient, sender, subject, msg_body, cc=None):
    try:
        hc = HealthCheck.get(email,event)
        if hc is not None and hc.notification_sent == True:
            time_delta = datetime.timedelta(days=1)
            # time_delta = datetime.timedelta(hours=12)
            expired_date = hc.notification_sent_date + time_delta
            if expired_date < datetime.datetime.now():
                hc.delete()
                hc = None

        if hc is None:
            params = { 'email': email, 'event': event }
            hc = HealthCheck.create(params)

        if hc.notification_sent == False:
            if cc:
                mail.send_mail(sender=sender, to=recipient, cc=cc, subject=subject, body=msg_body, html=msg_body)
            else:
                mail.send_mail(sender=sender, to=recipient, subject=subject, body=msg_body, html=msg_body)
            hc.notification_sent = True
            hc.notification_sent_date = datetime.datetime.now()
            hc.put()

    except Exception as e:
        logging.exception('Failed sending Health Check Email')
