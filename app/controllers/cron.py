import logging
import requests
import yaml
import os
from app.mails import SendMail
from google.appengine.ext import deferred
from ferris import Controller, route
from app.settings import settings
from requests.exceptions import ConnectionError

PORTAL_URL = settings['chicos_urls']['portal']
SCORECARD_URL = settings['chicos_urls']['scorecard']

class Cron(Controller):
    @route
    def health_check(self):
        deferred.defer(health_check_job)
        return "Executing Health Check in the background"


def health_check_job():
    settings,buff = None,""
    login, notifications = get_settings()
    deferred.defer(health_check1, login, notifications)

def get_settings():
    path = '/'.join(os.path.split(__file__)[0].split('/')[:-1])
    filename = os.path.join(path, "settings.yaml")
    with open(filename,"r") as f:
        buff = f.read()
        settings = yaml.load(buff)
        f.close()
    return settings['login'],settings['notifications']

def health_check1(login, notifs):
    try:
        for cred in login:
            session, log_resp = send_login_session(cred['user'], cred['email'], cred['password'], notifs)
            if session is None: return
            content_resp = log_resp.content
            if "This account's password has expired" in content_resp.lower():
                logging.warning("Account Password Expired")
                email,cc = get_email('password_expired', notifs)
                SendMail('password_expired', email=email, username=cred['user'], cc=cc)
            elif '/users/login' in content_resp.lower():
                logging.warning("Account Access Denied")
                email,cc = get_email('account_inaccessible', notifs)
                SendMail('account_inaccessible', email=email, username=cred['user'], content=content_resp, cc=cc)
            elif '/users/logout' in content_resp.lower():
                logging.info("Health Check 1 completed")
                deferred.defer(health_check2, session, notifs)
                deferred.defer(health_check3, session, notifs, cred['user'], content_resp)
                deferred.defer(health_check4, session, notifs)
            else:
                logging.info("Health Check 1 unsuccessful. Send error notification email")
                email,cc = get_email('portal_inaccessible', notifs)
                SendMail('portal_inaccessible', email=email, content=content_resp, cc=cc)

    except Exception as e:
        logging.exception("Encountered error on health check 1")

def health_check2(session, notifs):
    try:
        pages_resp = send_pages_session(session, notifs)
        if pages_resp.status_code == 200:
            pages_json = pages_resp.json()
            if 'children' in pages_json and len(pages_json['children'])>0:
                logging.info("Health Check 2 completed %s children found"%len(pages_json['children']))
            else:
                logging.info("Health Check 2 unsuccessful. Send error notification email")
                email,cc = get_email('portal_pages_not_found', notifs)
                SendMail('portal_pages_not_found', email=email, cc=cc)
    except Exception as e:
        logging.exception("Encountered error on health check 2")

def health_check3(session, notifs, username, log_content):
    try:
        sc_resp = send_scorecard_home_session(session, notifs)
        content_resp = sc_resp.content
        if 'Supplier Reports' in content_resp:
            logging.info("Health Check 3 completed")
        elif log_content == content_resp:
            logging.warning("Account has no access to Scorecard Application")
            email,cc = get_email('account_inaccessible', notifs)
            SendMail('account_inaccessible', email=email, username=username, content=content_resp, cc=cc, site='scorecard')
        else:
            logging.info("Health Check 3 unsuccessful. Send error notification email")
            email,cc = get_email('scorecard_inaccessible', notifs)
            SendMail('scorecard_inaccessible', email=email, content=content_resp, cc=cc)

    except Exception as e:
        logging.exception("Encountered error on health check 3")

def health_check4(session, notifs):
    try:
        records_resp = send_records_session(session, notifs)
        if records_resp.status_code == 200:
            records_json = records_resp.json()
            if 'items' in records_json and len(records_json['items'])>0:
                logging.info("Health Check 4 completed. %s items found"%len(records_json['items']))
            else:
                logging.info("Health Check 4 unsuccessful. Send error notification email")
                email,cc = get_email('scorecard_records_not_found', notifs)
                SendMail('scorecard_records_not_found', email=email, cc=cc)
    except Exception as e:
        logging.exception("Encountered error on health check 2")


def send_login_session(username, email, password, notifs):
    s = requests.Session()
    data = [('username',username,),('password',password)]
    s.headers.update({'Content-Type': 'application/x-www-form-urlencoded'})
    try:
        resp = s.post(url=PORTAL_URL+'/users/login', data=data, timeout=5)
        return s, resp
    except requests.Timeout as e:
        log_timeout('send_login_session', 'portal', PORTAL_URL+'/users/login', notifs)
    except ConnectionError as ce:
        log_timeout('send_login_session', 'portal', PORTAL_URL+'/users/login', notifs)
    return None, None

def send_pages_session(session, notifs):
    try:
        resp = session.get(url=PORTAL_URL+'/api/pages/tree/none?depth=1', timeout=5)
        return resp
    except requests.Timeout as e:
        log_timeout('send_pages_session', 'portal', PORTAL_URL+'/api/pages/tree/none?depth=1', notifs)
    except ConnectionError as ce:
        log_timeout('send_pages_session', 'portal', PORTAL_URL+'/api/pages/tree/none?depth=1', notifs)

def send_scorecard_home_session(session, notifs):
    try:
        resp = session.get(url=SCORECARD_URL, timeout=5)
        return resp
    except requests.Timeout as e:
        log_timeout('send_scorecard_home_session', 'scorecard', SCORECARD_URL, notifs)
    except ConnectionError as ce:
        log_timeout('send_scorecard_home_session', 'scorecard', SCORECARD_URL, notifs)

def send_records_session(session, notifs):
    try:
        resp = session.get(url=SCORECARD_URL+'/api/records/sort:-uploaded_date', timeout=5)
        return resp
    except requests.Timeout as e:
        log_timeout('send_records_session', 'scorecard', SCORECARD_URL+'/api/records/sort:-uploaded_date', notifs)
    except ConnectionError as ce:
        log_timeout('send_records_session', 'scorecard', SCORECARD_URL+'/api/records/sort:-uploaded_date', notifs)

def log_timeout(func, site, url, notifs):
    logging.exception("Timeout encountered on %s"%func)
    email, cc = get_email('request_timeout', notifs)
    SendMail('request_timeout', email=email, cc=cc, site=site, url=url)

def get_email(event=None, notifs=None):
    email, cc = None, None
    if event is not None and notifs is not None:
        event_item = [s for s in notifs if event == s['event']]
        email, cc = event_item[0]['email'], event_item[0]['cc'].split(',') if 'cc' in event_item[0] else None
    return email, cc
