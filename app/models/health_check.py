from ferris import BasicModel, ndb
import logging

class HealthCheck(BasicModel):
    email = ndb.StringProperty(indexed=True, required=True)
    event = ndb.StringProperty(indexed=True, default=None)
    notification_sent = ndb.BooleanProperty(indexed=True, default=False)
    notification_sent_date = ndb.DateTimeProperty(indexed=True)

    @classmethod
    def list(cls):
        return cls.query().order(cls.name)

    @classmethod
    def create(cls, params):
        item = cls()
        item.populate(**params)
        item.put()
        return item

    @classmethod
    def get(cls, email, event):
        result = cls.query(cls.email==email, cls.event==event).order(-cls.created).fetch(1)
        return result[0] if result and len(result) else None
